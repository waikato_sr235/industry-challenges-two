package intarrayrotation;

/**
 * Rotates an array, moving each element to current index - 1, and the element at index 0 to the final position
 * @author Sam Rosenberg
 */
public class ArrayRotator {
    /**
     * Rotates an integer array by moving each element to current index - 1, and the element at index 0 to the final position
     * @param toRotate int array
     * @return rotated array
     */
    public int[] rotate(int[] toRotate) {
        int[] result = new int[toRotate.length];

        int first = toRotate[0];
        result[result.length - 1] = first;

        for (int i = 1; i < toRotate.length; i++) {
            result[i - 1] = toRotate[i];
        }
        return result;
    }
}
