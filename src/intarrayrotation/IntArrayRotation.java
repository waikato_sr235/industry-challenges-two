package intarrayrotation;

import java.util.Scanner;

/**
 * A program that prompts the user to enter string representation of an array of int arrays then rotates the contents and prints them back to the user
 * @author Sam Rosenberg
 */
public class IntArrayRotation {
    final private IntArrayStringConverter converter;
    final private ArrayRotator rotator;

    public static void main(String[] args) {
        IntArrayRotation app = new IntArrayRotation();
        app.start();
    }

    private IntArrayRotation() {
        converter = new IntArrayStringConverter();
        rotator = new ArrayRotator();
    }

    private void start() {
        String userInput = getStringFromUser();

        String output = rotateStringContents(userInput);

        printResult(userInput, output);
    }

    private String getStringFromUser() {
        System.out.println("Please enter the array(s) you wish to rotate in the format " + converter.formatGuide());

        Scanner input = new Scanner(System.in);
        String userInput = input.nextLine();

        while (!converter.isValidString(userInput)) {
            System.out.println("That was not a valid string. Please enter string in the specified format");
            userInput = input.nextLine();
        }
        return userInput;
    }

    private String rotateStringContents(String userInput) {
        int[][] intArrays = converter.stringToArrayOfIntArrays(userInput);

        for (int i = 0; i < intArrays.length; i++) {
            intArrays[i] = rotator.rotate(intArrays[i]);
        }

        return converter.arrayOfIntArraysToString(intArrays);
    }

    private void printResult(String userInput, String rotatedArray) {
        System.out.println("Before: " + userInput);
        System.out.println("After being rotated: ");
        System.out.println(rotatedArray);
    }


}
