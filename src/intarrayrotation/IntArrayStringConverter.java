package intarrayrotation;

/**
 * A class that converts between Strings and arrays of int Arrays
 * A string must be in the format "n1, n2 ... , n3|n1 ... , n3" to be converted to an int array
 * Int arrays will be returned in this format; single arrays can be entered but must be contained within an array of length 1
 * Empty arrays cannot be converted and isValidString will return false if a string contains an empty array
 * @author Sam Rosenberg
 */
public class IntArrayStringConverter {

    /**
     * Validates whether a given string is in a format that this class can work with
     * @param toValidate any String
     * @return true if valid
     */
    public boolean isValidString(String toValidate) {
        return toValidate.matches("^-?\\d+(, -?\\d+)*(\\|-?\\d+(, -?\\d+)*)*");
    }

    /**
     *
     * @return a string that describes the format IntArrayStringConverter is expecting/isValidString validates for
     */
    public String formatGuide() {
        return "n1, n2..., n3|n1..., n3";
    }

    /**
     * Converts a string to an array or int arrays
     * @param toConvert should be validated with isValidString before being passed into this method
     * @return an array; if a string with a single array is passed it will return an array of length 1 containing that array
     */
    public int[][] stringToArrayOfIntArrays(String toConvert) {
        String[] arrayReps = toConvert.split("\\|");
        int[][] result = new int[arrayReps.length][];

        for (int i = 0; i < result.length; i++) {
            result[i] = stringToIntArray(arrayReps[i]);
        }
        return result;
    }

    /**
     * Converts an array of ints to a string following the format definitions of this class
     * @param toConvert an array of int arrays; a single array should be passed in as a member of a 1 length array
     * @return String following this class's formatting requirements
     */
    public String arrayOfIntArraysToString(int[][] toConvert) {
        StringBuilder result = new StringBuilder();
        for (int[] array : toConvert) {
            for (int member : array) {
                result.append(member).append(", ");
            }
            result.delete(result.length() - 2, result.length());
            result.append("|");
        }
        result.delete(result.length() - 1, result.length());
        return result.toString();
    }

    /**
     * This method will not validate its input - make sure you validate before passing in a string
     * Used by stringToArrayOfIntArrays()
     * @param toConvert string in the format n1, n2, n3... where each n is a valid integer
     * @return an array of ints
     */
    private int[] stringToIntArray(String toConvert) {
        String[] arrayMembers = toConvert.split(",");
        int[] result = new int[arrayMembers.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(arrayMembers[i].strip());
        }
        return result;
    }
}
