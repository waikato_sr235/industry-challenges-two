package intarrayrotation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Copied & adapted from the TestElapsedTime unit tests in Challenge 1
 */
class TestIntArrayStringConverter {
    private IntArrayStringConverter cr;
    private ArrayRotator rotator;

    @BeforeEach
    void setUp(){
        cr = new IntArrayStringConverter();
        rotator = new ArrayRotator();
    }

    @Test
    public void testFormatGuide() {
        //depending on how you like to write format guides, this might need to be tweaked, but it mostly serves to make sure that if you change the validation regex the formatGuide() method must change too
        assertTrue(cr.isValidString(cr.formatGuide().replaceAll("n","").replaceAll("\\.\\.\\.", "")));
    }

    @Test
    public void testIsValidString_StringIsValid_SingleDigitInts() {
        assertTrue(cr.isValidString("1, 2, 3"));
        assertTrue(cr.isValidString("1, 0, 3|1, 2, 3"));
        assertTrue(cr.isValidString("0, 2, 3|4, 5, 6|1, 2, 3"));
        assertTrue(cr.isValidString("1, 2, 3|9, 4, 0, 1|0, 2, 3, 8, 4|9, 4, 1"));
    }

    @Test
    public void testIsValidString_StringIsValid_SingleIntArray() {
        assertTrue(cr.isValidString("1"));
        assertTrue(cr.isValidString("1|1|1"));
        assertTrue(cr.isValidString("1, 2, 3|3|1, 2, 3"));
        assertTrue(cr.isValidString("1, 2, 3|4, 5, 6|7"));
    }

    @Test
    public void testIsValidString_StringIsValid_NegativeNumber() {
        assertTrue(cr.isValidString("-1, 2, 3|1, 2, 3|4, 8, 1"));
        assertTrue(cr.isValidString("1, -2, 3|-1, 2, 3|4, 8, 1"));
        assertTrue(cr.isValidString("1, 2, 3|1, -2, 3|4, 8, 1"));
        assertTrue(cr.isValidString("1, 2, 3|1, 2, 3|4, 8, 1"));
    }

    @Test
    public void testIsValidString_StringIsValid_MultipleDigits() {
        assertTrue(cr.isValidString("12341, 2, 3, 4|1, 2, 8"));
        assertTrue(cr.isValidString("1, 234, 5|6, 7, 8"));
        assertTrue(cr.isValidString("1, 2, 3|123, 4, 5"));
        assertTrue(cr.isValidString("1, 2, 3|4, 567, 8"));
    }

    @Test
    public void testIsValidString_StringIsInvalid_NonDigit() {
        assertFalse(cr.isValidString("a, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3|4, a, 6"));
        assertFalse(cr.isValidString("1, 2, 3|4, 5, a"));
        assertFalse(cr.isValidString("1, a, 3|4, 5, 6"));
    }

    @Test
    public void testIsValidString_StringIsInvalid_MixedNonDigitAndDigit() {
        assertFalse(cr.isValidString("1a, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("a1, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1a1, 2, 3|1, 2, 3"));

        assertFalse(cr.isValidString("2, 1a, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1, a1, 3|4, 5, 6"));
        assertFalse(cr.isValidString("3, 1a1, 2, 3|1, 2, 3"));

        assertFalse(cr.isValidString("1, 2, 3|1a, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3|a1, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3|1a1, 2, 3|1, 2, 3"));

        assertFalse(cr.isValidString("1, 2, 3|1, 1a, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3|1, a1, 2, 3|4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3|1, 1a1, 2, 3|1, 2, 3"));
    }

    @Test
    public void testIsValidString_StringIsInvalid_IncorrectPunctuation() {
        assertFalse(cr.isValidString("1. 2. 3|4. 5. 6"));
        assertFalse(cr.isValidString("1,2,3|4,5,6"));
        assertFalse(cr.isValidString("1, 2, 3, |4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3;4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3:4, 5, 6"));
        assertFalse(cr.isValidString("1, 2, 3|"));
        assertFalse(cr.isValidString("|1, 2, 3"));
    }

    @Test
    public void testIsValidString_StringIsInvalid_ContainsEmptyArray() {
        assertFalse(cr.isValidString("1, 2, 3||4, 5, 6"));
        assertFalse(cr.isValidString("||1, 2, 3"));
        assertFalse(cr.isValidString("1, 2, 3||"));
    }

    @Test
    public void testArrayOfIntArraysToString_OutputIsValid() {
        assertTrue(cr.isValidString(cr.arrayOfIntArraysToString(new int[][]{{1, 2, 3},{9, 4, 6, 1},{1, 2, 3, 8, 4},{9, 4, 1}})));
        assertTrue(cr.isValidString(cr.arrayOfIntArraysToString(new int[][]{{1, -2, 3},{-1, 2, 3},{4, -8, -1}})));
        assertTrue(cr.isValidString(cr.arrayOfIntArraysToString(new int[][]{{1},{1},{1}})));
        assertTrue(cr.isValidString(cr.arrayOfIntArraysToString(new int[][]{{1234, 234, 5678},{6546, 75675, 436818}})));
    }

    @Test
    public void testArrayOfIntArraysToString_OutputMatchesInput() {
        assertEquals("1, 2, 3|9, 4, 6, 1|1, 2, 3, 8, 4|9, 4, 1", cr.arrayOfIntArraysToString(new int[][]{{1, 2, 3},{9, 4, 6, 1},{1, 2, 3, 8, 4},{9, 4, 1}}));
        assertEquals("1, -2, 3|-1, 2, 3|4, -8, -1", cr.arrayOfIntArraysToString(new int[][]{{1, -2, 3},{-1, 2, 3},{4, -8, -1}}));
        assertEquals("1|1|1", cr.arrayOfIntArraysToString(new int[][]{{1},{1},{1}}));
        assertEquals("1234, 234, 5678|6546, 75675, 436818", cr.arrayOfIntArraysToString(new int[][]{{1234, 234, 5678},{6546, 75675, 436818}}));
    }
    @Test
    public void testStringToArrayOfIntArrays() {
        assertArrayEquals(new int[][]{{1, 2, 3},{9, 4, 6, 1},{1, 2, 3, 8, 4},{9, 4, 1}}, cr.stringToArrayOfIntArrays("1, 2, 3|9, 4, 6, 1|1, 2, 3, 8, 4|9, 4, 1"));
        assertArrayEquals(new int[][]{{1, -2, 3},{-1, 2, 3},{4, -8, -1}}, cr.stringToArrayOfIntArrays("1, -2, 3|-1, 2, 3|4, -8, -1"));
        assertArrayEquals(new int[][]{{1},{1},{1}}, cr.stringToArrayOfIntArrays("1|1|1"));
        assertArrayEquals(new int[][]{{1234, 234, 5678},{6546, 75675, 436818}}, cr.stringToArrayOfIntArrays("1234, 234, 5678|6546, 75675, 436818"));
    }

    @Test
    public void testRotate() {
        assertArrayEquals(new int[]{5, 1, 6, 2, 4}, rotator.rotate(new int[]{4, 5, 1, 6, 2}));
        assertArrayEquals(new int[]{6}, rotator.rotate(new int[]{6}));
        assertArrayEquals(new int[]{-1, 2, 9}, rotator.rotate(new int[]{9, -1, 2}));
        assertArrayEquals(new int[]{438950, 982, 2138}, rotator.rotate(new int[]{2138, 438950, 982}));
    }
}